#include <iostream>
#include "parser.h"
Parser::Parser()
{
	this->registro_aeropuerto.cantidad_terminales = 0;
	this->registro_aeropuerto.ciudad = " ";
	this->registro_aeropuerto.destino_internacionales = 0;
	this->registro_aeropuerto.destino_nacionales = 0;
	this->registro_aeropuerto.nombre_aeropuerto = " ";
	this->registro_aeropuerto.pais = " ";
	this->registro_aeropuerto.superficie = 0;
	this->codigo_aeropuerto = " ";
	this->linea = " ";
	this->archivo.open("aeropuertos.txt",ios::in);
}
string Parser::obtenerCodigo()
{
	return(this->codigo_aeropuerto);
}
DatosAeropuerto Parser::obtenerDatosDelAeropuerto()
{
	string aux2;
	char codigo[4];
	bool es_fin_de_archivo = true;
	setCodigoAeropuerto("0");
	this->registro_aeropuerto.cantidad_terminales = 0;
	this->registro_aeropuerto.ciudad = " ";
	this->registro_aeropuerto.destino_internacionales = 0;
	this->registro_aeropuerto.destino_nacionales = 0;
	this->registro_aeropuerto.nombre_aeropuerto = " ";
	this->registro_aeropuerto.pais = " ";
	this->registro_aeropuerto.superficie = 0;
	leerLinea(es_fin_de_archivo);
	if(es_fin_de_archivo == false)
	{
		pasarCodigo(codigo,this->linea);
		this->codigo_aeropuerto = codigo;
		parseoGeneral();
	}
	return(registro_aeropuerto);
}
void Parser::leerLinea(bool &es_fin)
{
	if (this->archivo.is_open() == true)
	{
		es_fin = this->archivo.eof();
		if (this->archivo.eof() == false)
		{
			getline(this->archivo,this->linea);
			cout<<"Linea leida"<<endl;
		}
		else
		{
			cout<<"Fin del archivo!"<<endl;
			this->archivo.close();
		}
	}
	else
	{
		cout<<"No existe dicho archivo."<<endl;
		this->linea = " ";
	}
}
void Parser::setCodigoAeropuerto(string codigo)
{
	this->codigo_aeropuerto = codigo;
}
void Parser::parseoGeneral()
{
	int i;
	i = 4;
	registro_aeropuerto.nombre_aeropuerto = pasarCadenas(i);
	i++;
	registro_aeropuerto.ciudad = pasarCadenas(i);
	i++;
	registro_aeropuerto.pais = pasarCadenas(i);
	i++;
	string auxiliar_numeros;
	auxiliar_numeros = pasarCadenas(i);
	this->registro_aeropuerto.superficie = atof(auxiliar_numeros.c_str());
	i++;
	auxiliar_numeros = pasarCadenas(i);
	this->registro_aeropuerto.cantidad_terminales = atoi(auxiliar_numeros.c_str());
	i++;
	auxiliar_numeros = pasarCadenas(i);
	this->registro_aeropuerto.destino_nacionales = atoi(auxiliar_numeros.c_str());
	i++;
	auxiliar_numeros = pasarCadenasFinal(i);
	this->registro_aeropuerto.destino_internacionales = atoi(auxiliar_numeros.c_str());
}
void Parser::pasarCodigo(char codigo[4],string auxiliar)
{
	int i = 0;
	while (auxiliar[i] != ' ')
	{
		codigo[i] = auxiliar[i];
		i++;
	}
}
string Parser::pasarCadenas(int &pos1)
{
	string auxiliar;
	int contador = 0;
	int j = pos1,i;
	while (linea[pos1] != ' ')
	{
		contador++;
	    pos1++;
	}
	contador++;
	char cadena[contador];
	i = 0;
	while (linea[j] != ' ')
	{
		cadena[i] = linea[j];
		i++;
		j++;
	 }
	cadena[i] = '\0';
	auxiliar = cadena;
	return(auxiliar);
}
string Parser::pasarCadenasFinal(int &pos1)
{
	string auxiliar;
	char auxiliar2[7];
	int i = 0;
	while (this->linea[pos1] != '\0')
	{
		auxiliar2[i] = this->linea[pos1];
		pos1++;
		i++;
	}
	auxiliar2[i] = '\0';
	auxiliar = auxiliar2;
	return(auxiliar);
}

#ifndef NODO_ARBOL_H_
#define NODO_ARBOL_H_
#include "datos_de_aeropuerto.h"
#include <string>
using std::string;
class NodoArbol
{
private:
	string codigo_iata; //Esta es la clave
	DatosAeropuerto* punteroDatosAeropuerto;
	NodoArbol* pizquierdo;
	NodoArbol* pderecho;
public:
	//pre:-
	//post: El nodo fue creado
	NodoArbol();
	//pre: recibe una cadena y un dato
	//post: el nodo del arbol fue creado
	NodoArbol(string codigo_ingresado,DatosAeropuerto* ptrAeropuerto);
	//pre: el nodo debe existir
	//post: carga la cadena codigo_iata
	void set_codigo(string codigo_ingresado);
	//pre: el nodo debe existir
	//post:carga punteroDatosAeropuerto
	void set_ptrDatosAeropuerto(DatosAeropuerto* ptrAeropuerto);
	 //pre: el nodo debe existir
	 //post: devuelve una cadena
	string get_codigo();
	  //pre: el nodo debe existir
	  //post: devuelve un dato
	DatosAeropuerto* get_ptrDatosAeropuerto();
	 //pre:el nodo debe existir
	 //post: se cargo ese nodo en pizquierdo
	void set_izquierdo(NodoArbol* ps);
	//pre:el nodo debe existir y recibe otro nodo
	//post: se cargo ese nodo en pderecho
	void set_derecho(NodoArbol* ps);
	//pre:el nodo debe existir y recibe otro nodo
	//post:devuelve otro nodo
	NodoArbol* get_izquierdo();
	 //pre: el nodo debe existir
	 //post:devuelve otro nodo
	NodoArbol* get_derecho();
	//pre:el nodo debe existir
	//post: el nodo fue destruido
	~NodoArbol();
};
#endif /* NODO_ARBOL_H_ */

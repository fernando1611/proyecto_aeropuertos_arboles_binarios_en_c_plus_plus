#ifndef ARBOL_BINARIO_H_
#define ARBOL_BINARIO_H_
#include "nodo_arbol.h"
#include <string>
class ArbolBinario
{
private:
	NodoArbol* raiz;
public:
	//pre:-
	//post:el arbol fue creado
	ArbolBinario();
	//pre: el arbol debe existir
	//post: devuelve un bolleano
	bool arbol_vacio();
	//pre:el arbol debe existir y recibe un dato y una cadena con la clave
	//post: se guardo la clave con su dato en el arbol
	void alta(DatosAeropuerto* ptrAeropuerto,string codigo_iata_ingresado);
	//pre:el arbol debe existir y recibe una cadena
	 //post:elimino el dato asociado a la clave si existia
	void baja(string iata_ingresado);
	//pre:el arbol debe existir y recibe una cadena
	//post: devuelve el dato asociado a la clave si existe y NULL si no existe
	DatosAeropuerto* busqueda(string codigo_iata_ingresado);
	//pre:el arbol debe existir
	//post: el arbol fue eliminado
	~ArbolBinario();
private:
	//pre: el arbol debe existir, recibe un nodo y una cadena
	//post: elimina un nodo hoja
	void eliminarCaso1(NodoArbol* &padre,string codigo);
	//pre: el arbol debe existir, recibe un nodo y una cadena
	//post: elimina un nodo que posee un hijo
	void eliminarCaso2(NodoArbol* &padre,string codigo);
	 //pre: el arbol debe existir, recibe un nodo y una cadena
	 //post: elimina un nodo que posee dos hijos
	void eliminarCaso3(NodoArbol* &padre,string codigo);
	//Pre: el arbol debe existir y se recibe una cadena
	//Post: Devuelve el padre del nodo que contiene la clave a buscar
	NodoArbol* encontrarPadre(string codigo);
	//Pre: El arbol debe existir y recibe un nodo
	//post: Retorna el padre del mayor de los menores
	NodoArbol* pPadreMayorMenores(NodoArbol* paux);
	//pre: El arbol debe existir y recibe un nodo
	//post: elimina todos los nodos
	void podar(NodoArbol* nodo);
};
#endif /* ARBOL_BINARIO */

#include <iostream>
#include "nodo_arbol.h"
using namespace std;
NodoArbol::NodoArbol()
{
	this->codigo_iata = " ";
	this->punteroDatosAeropuerto = 0;
	this->pderecho = 0;
	this->pizquierdo = 0;
}
NodoArbol::NodoArbol(string codigo_ingresado,DatosAeropuerto* ptrAeropuerto)
{
	cout<<"Se construye el nodo "<<this<<endl;
	this->codigo_iata = codigo_ingresado;
	this->punteroDatosAeropuerto = ptrAeropuerto;
	this->pizquierdo = 0;
	this->pderecho = 0;
}
void NodoArbol::set_codigo(string codigo_ingresado)
{
	this->codigo_iata = codigo_ingresado;
}
void NodoArbol::set_ptrDatosAeropuerto(DatosAeropuerto* ptrAeropuerto)
{
	this->punteroDatosAeropuerto = ptrAeropuerto;
}
string NodoArbol::get_codigo()
{
	return(this->codigo_iata);
}
DatosAeropuerto* NodoArbol::get_ptrDatosAeropuerto()
{
	return(this->punteroDatosAeropuerto);
}
void NodoArbol::set_izquierdo(NodoArbol* ps)
{
	this->pizquierdo = ps;
}
void NodoArbol::set_derecho(NodoArbol* ps)
{
	this->pderecho = ps;
}
NodoArbol* NodoArbol::get_izquierdo()
{
	return(this->pizquierdo);
}
NodoArbol* NodoArbol::get_derecho()
{
	return(this->pderecho);
}
NodoArbol::~NodoArbol()
{
	cout<<"Se destruye el nodo "<<this<<endl;
}

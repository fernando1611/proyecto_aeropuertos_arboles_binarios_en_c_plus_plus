#include <iostream>
#include "arbol_binario.h"
using namespace std;
ArbolBinario::ArbolBinario()
{
	this->raiz = 0;
}
bool ArbolBinario::arbol_vacio()
{
	return(this->raiz == 0);
}
//version iterativa
void ArbolBinario::alta(DatosAeropuerto* ptrAeropuerto,string codigo_iata_ingresado)
{
	NodoArbol* pnodonuevo = new NodoArbol(codigo_iata_ingresado,ptrAeropuerto);
	NodoArbol* aux = this->raiz;
	NodoArbol* anterior;
	while (aux != 0)
	{
		anterior = aux;
		if (codigo_iata_ingresado < aux->get_codigo())
			aux = aux->get_izquierdo();
		else
			aux = aux->get_derecho();
	}
	if (this->arbol_vacio() == true)
		this->raiz = pnodonuevo;
	else
	{
		if (codigo_iata_ingresado < anterior->get_codigo())
			anterior->set_izquierdo(pnodonuevo);
		else
			anterior->set_derecho(pnodonuevo);
	}
}
void ArbolBinario::baja(string iata_ingresado)
{
	NodoArbol* padre;
	if (this->arbol_vacio() != true)
	{
		// Caso en que el nodo a eliminar es la raiz
		if (iata_ingresado == this->raiz->get_codigo())
		{
			if ((this->raiz->get_izquierdo() == 0) && (this->raiz->get_derecho()))
				eliminarCaso1(this->raiz,iata_ingresado);
			else
			{
				if (((raiz->get_izquierdo() == 0) && (raiz->get_derecho() != 0)) || ((raiz->get_izquierdo() != 0) && (raiz->get_derecho() == 0)))
					eliminarCaso2(this->raiz,iata_ingresado);
				else
					eliminarCaso3(this->raiz,iata_ingresado);
			}
		}
		else
		{
			padre = encontrarPadre(iata_ingresado);
			if (padre != 0) //Si el padre es nulo entonces la clave no esta en el ABB
			{
				NodoArbol* pnodo;
				if (iata_ingresado>padre->get_codigo())
					pnodo = padre->get_derecho();
				else
					pnodo = padre->get_izquierdo();
				if ((pnodo->get_izquierdo() == 0) && (pnodo->get_derecho() == 0))
					eliminarCaso1(padre,iata_ingresado);
				else
				{
					if (((pnodo->get_izquierdo() != 0) && (pnodo->get_derecho() == 0)) || ((pnodo->get_izquierdo() == 0) && (pnodo->get_derecho() != 0)))
						eliminarCaso2(padre,iata_ingresado);
					else
						eliminarCaso3(padre,iata_ingresado);
				}
			}
			else
				cout<<"El nodo EXISTE!!!!"<<endl;
		}
	}
	else
		cout<<"El arbol esta vacio, no hay elementos"<<endl;
}
//version iterativa
DatosAeropuerto* ArbolBinario::busqueda(string codigo_iata_ingresado)
{
	NodoArbol* aux;
	bool encontrado;
	encontrado = false;
	if (this->arbol_vacio() != true)
	{
		aux = this->raiz;
		while ((aux != 0) && (encontrado == false))
		{
			if (codigo_iata_ingresado == aux->get_codigo())
				encontrado = true;
			else
			{
				if (codigo_iata_ingresado>aux->get_codigo())
					aux = aux->get_derecho();
				else
					aux = aux->get_izquierdo();
			}
		}
	}
	if (encontrado == true)
		return(aux->get_ptrDatosAeropuerto());
	else
		return(0);
}
ArbolBinario::~ArbolBinario()
{
	podar(this->raiz);
	cout<<"El arbol fue destruido en "<<this<<endl;
}
//Cuando el nodo a eliminar es una hoja
void ArbolBinario::eliminarCaso1(NodoArbol* &padre,string codigo)
{
	NodoArbol* aux;
	//En el caso de que el padre sea la raiz del arbol o una situacion de eliminarCaso3
	if ((padre->get_izquierdo() == 0) && (padre->get_derecho() == 0))
			aux = padre;
	else
	{
		if (codigo>padre->get_codigo())
		{
			//Si la hoja a eliminar esta en la derecha
			aux = padre->get_derecho();
			padre->set_derecho(0);
		}
		else
		{
			//Si la hoja a eliminar esta en la izquierda
			aux = padre->get_izquierdo();
			padre->set_izquierdo(0);
		}
	}
	delete aux->get_ptrDatosAeropuerto();
	delete aux;
}
//Cuando el nodo a eliminar tiene un hijo
void ArbolBinario::eliminarCaso2(NodoArbol* &padre,string codigo)
{
	NodoArbol* aux;
	if (padre == this->raiz)
	{
		//cuando el nodo a borrar es la misma raiz
		aux = padre;
		if (padre->get_izquierdo() != 0)
			padre = aux->get_izquierdo();
		else
			padre = aux->get_derecho();
	}
	else
	{
		if (codigo>padre->get_codigo())
		{
			aux = padre->get_derecho();
			if (aux->get_derecho() == 0)
				padre->set_derecho(aux->get_izquierdo());
			else
				padre->set_derecho(aux->get_derecho());
		}
		else
		{
			aux = padre->get_izquierdo();
			if (aux->get_derecho() == 0)
				padre->set_izquierdo(aux->get_izquierdo());
			else
				padre->set_izquierdo(aux->get_derecho());
		}
	}
	delete aux->get_ptrDatosAeropuerto();
	delete aux;
}
void ArbolBinario::eliminarCaso3(NodoArbol* &padre,string codigo)
{
	NodoArbol *aux,*aux2;
	NodoArbol* mayor_de_los_menores; //Para inicializar
	if (codigo>padre->get_codigo())

		aux = padre->get_derecho();
	else
		aux = padre->get_izquierdo();
	//En aux se guarda el nodo a eliminar
	aux2 = pPadreMayorMenores(aux); //aux2 sera el padre del mayor de los menores
	if (aux2->get_derecho() == 0)
	{
		//caso 1: Cuando aux2 no tiene nodo derecho y en consecuencia es el mayor de los menores
		aux->set_codigo(aux2->get_codigo());
		aux->set_ptrDatosAeropuerto(aux2->get_ptrDatosAeropuerto());
		aux->set_izquierdo(aux2->get_izquierdo());
		delete aux2->get_ptrDatosAeropuerto();
		delete aux2;
	}
	else
	{
		mayor_de_los_menores = aux2->get_derecho();
		aux->set_codigo(mayor_de_los_menores->get_codigo());
		aux->set_ptrDatosAeropuerto(mayor_de_los_menores->get_ptrDatosAeropuerto());
		if (mayor_de_los_menores->get_izquierdo() == 0)
			//Caso 2: Cuando aux2 es el padre del mayor de los menores pero este mismo no tiene nodo izquierdo
			eliminarCaso1(mayor_de_los_menores,mayor_de_los_menores->get_codigo());
		else
		{
			//Caso 3: Cuando aux3 es el padre del mayor de los menores pero este si tiene nodo izquierdo
			aux->set_derecho(mayor_de_los_menores->get_izquierdo());
			delete mayor_de_los_menores->get_ptrDatosAeropuerto();
			delete mayor_de_los_menores;
		}
	}
}
NodoArbol* ArbolBinario::encontrarPadre(string codigo)
{
	//Si el elemento no existe, padre igual a null
	NodoArbol* padre;
	padre = this->raiz;
	bool padre_encontrado = false;
	bool no_hay_padre = false;
	while ((padre_encontrado == false) && (no_hay_padre == false))
	{
		if (codigo>padre->get_codigo())
		{
			if (padre->get_derecho() != 0)
			{
				if (codigo == padre->get_derecho()->get_codigo())
					padre_encontrado = true;
				else
					padre = padre->get_derecho();
			}
			else
			{
				no_hay_padre = true;
				padre = 0;
			}
		}
		else
		{
			if (padre->get_izquierdo() != 0)
			{
				if (codigo == padre->get_izquierdo()->get_codigo())
					padre_encontrado = true;
				else
					padre = padre->get_izquierdo();
			}
			else
			{
				no_hay_padre = true;
				padre = 0;
			}
		}
	}
	return(padre);
}
NodoArbol* ArbolBinario::pPadreMayorMenores(NodoArbol* paux)
{
	NodoArbol* padre;
	bool padre_encontrado = false;
	padre = paux->get_izquierdo();
	while (padre->get_derecho() != 0 && (padre_encontrado == false)) //Si no hay padre mas a la derecha tomamos como mismo que guardamos incialmente
	{
		if (padre->get_derecho()->get_derecho() != 0)
			padre = padre->get_derecho();
		else
			padre_encontrado = true;
	}
	return(padre);
}
void ArbolBinario::podar(NodoArbol* nodo)
{
	if (nodo != 0)
	{
		delete nodo->get_ptrDatosAeropuerto();
		podar(nodo->get_izquierdo());
		podar(nodo->get_derecho());
		delete nodo;
	}
}

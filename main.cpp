#include <iostream>
#include <string>
#include "datos_de_aeropuerto.h"
#include "parser.h"
#include "arbol_binario.h"
using namespace std;
void cargar_arbol(Parser &parser,ArbolBinario &arbol)
{
	DatosAeropuerto* aux_aeropuerto;
	DatosAeropuerto registro_auxiliar;
	string codigo_aeropuerto;
	codigo_aeropuerto = "1"; //valor para incializar y entrar en el bucle
	while (codigo_aeropuerto != "0") //Cuando el codigo del aeropuerto es igual a cero, significa que nos encontramos en fin de archivo
	{
		registro_auxiliar = parser.obtenerDatosDelAeropuerto();
		codigo_aeropuerto = parser.obtenerCodigo();
		if (codigo_aeropuerto != "0")
		{
			aux_aeropuerto = new DatosAeropuerto;
			*aux_aeropuerto = registro_auxiliar;
			arbol.alta(aux_aeropuerto,codigo_aeropuerto);
		}
	}
}
void mostrar_datos(DatosAeropuerto* aeropuerto,string codigo)
{
	if (aeropuerto == 0)
		cout<<"No existen los datos buscados."<<endl;
	else
	{
		cout<<"Encontrados, los datos son: "<<endl;
		cout<<"Codigo IATA: "<<codigo<<endl;
		cout<<"Nombre del aeropuerto: "<<aeropuerto->nombre_aeropuerto<<endl;
		cout<<"Ciudad: "<<aeropuerto->ciudad<<endl;
		cout<<"Pais: "<<aeropuerto->pais<<endl;
		cout<<"Superficie: "<<aeropuerto->superficie<<endl;
		cout<<"cantidad de terminales: "<<aeropuerto->cantidad_terminales<<endl;
		cout<<"Cantidad de destinos internacionales: "<<aeropuerto->destino_internacionales<<endl;
		cout<<"Cantidad de destinos nacionales: "<<aeropuerto->destino_nacionales<<endl;
	}
}
void ingresar_datos(ArbolBinario &arbol,DatosAeropuerto &registro,DatosAeropuerto* &ptr,string iata)
{
	cout<<"Ingrese el codigo IATA"<<endl;
	cin>>iata;
	cout<<"Ingrese el nombre del aeropuerto (En los espacios, ingresar con '-'): "<<endl;
	cin>>registro.nombre_aeropuerto;
	cout<<"Ingrese su ciudad: "<<endl;
	cin>>registro.ciudad;
	cout<<"Ingrese el pais: "<<endl;
	cin>>registro.pais;
	cout<<"Ingrese la superficie de dicho aeropuerto: "<<endl;
	cin>>registro.superficie;
	cout<<"Ingrese la cantidad de terminales: "<<endl;
	cin>>registro.cantidad_terminales;
	cout<<"Ingrese la cantidad de destinos nacionales: "<<endl;
	cin>>registro.destino_nacionales;
	cout<<"Ingrese la cantidad de destinos internacionales: "<<endl;
	cin>>registro.destino_internacionales;
	ptr = new DatosAeropuerto;
	*ptr = registro;
	arbol.alta(ptr,iata);
}
void borrar_aeropuerto(ArbolBinario &arbol)
{
	string codigo_auxiliar;
	cout<<"Ingrese el codigo iata del aeropuerto a dar de baja: "<<endl;
	cin>>codigo_auxiliar;
	arbol.baja(codigo_auxiliar);
}
void menu_opciones(ArbolBinario &arbol)
{
	int opcion;
	char respuesta = 's';
	string codigo_iata;
	DatosAeropuerto registro_auxiliar;
	DatosAeropuerto* aux_aeropuerto;
	while(respuesta == 's')
	{
		cout<<"Bienvenido al menu."<<endl;
		cout<<"Por favor, ingrese una de las opciones validas: "<<endl;
		cout<<"1: Consultar por un aeropuerto en particular ingresando su codigo IATA."<<endl;
		cout<<"2: Dar de alta un nuevo aeropuerto ingresando los datos correspondientes."<<endl;
		cout<<"3: Dar de baja un aeropuerto ingresando su codigo IATA."<<endl;
		cin>>opcion;
		switch (opcion)
		{
			case 1:
				cout<<"Ingrese el codigo IATA a buscar: "<<endl;
				cin>>codigo_iata;
				aux_aeropuerto = arbol.busqueda(codigo_iata);
				mostrar_datos(aux_aeropuerto,codigo_iata);
				break;
			case 2:
				ingresar_datos(arbol,registro_auxiliar,aux_aeropuerto,codigo_iata);
				cout<<"Se han ingresado los datos de manera correcta."<<endl;
				break;
			case 3:
				borrar_aeropuerto(arbol);
				break;
			default:
				cout<<"Opcion incorrecta!"<<endl;
		}
		cout<<"Desea realizar mas operaciones? (s para si/ n para no): "<<endl;
		cin>>respuesta;
	}
}
int main()
{
	cout<<"Bienvenido, se cargaran todos sus registros de los aeropuertos en un arbol binario"<<endl;
	Parser objeto_parser;
	ArbolBinario objeto_arbol;
	cargar_arbol(objeto_parser,objeto_arbol);
	cout<<"Registros en el arbol cargados. Se mostrara el menu."<<endl;
	menu_opciones(objeto_arbol);
	return(0);
}

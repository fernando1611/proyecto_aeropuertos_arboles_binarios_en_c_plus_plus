#ifndef DATOS_DE_AEROPUERTO_H_
#define DATOS_DE_AEROPUERTO_H_
#include <string>
using std::string;
typedef struct
{
	string nombre_aeropuerto;
	string ciudad;
	string pais;
	float superficie;
	int cantidad_terminales;
	int destino_nacionales;
	int destino_internacionales;
}DatosAeropuerto;

#endif /* DATOS_DE_AEROPUERTO_H_ */
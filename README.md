# proyecto_aeropuertos_arboles_binarios_en_C_plus_plus

TP realizado en C++, hecho en base a arboles binarios de búsqueda. Dejo el enunciado aqui:
"El trabajo práctico 5 consiste en generar un Árbol Binario de Búsqueda (ABB) a través de la lectura de un
archivo de texto aeropuertos.txt. En cada línea estará la información de cada aeropuerto, separada por
espacios:
codigo_IATA nombre ciudad pais superficie cantidad_terminales destinos_nacionales destinos_internacionales
El código IATA es un código de tres letras que identifica a cada aeropuerto. Por ejemplo, EZE es el aeropuerto
Ministro Pistarini de Ezeiza, COR es el aeropuerto Pajas Blancas de Córdoba.
La superficie es un número flotante que indica los km2. Los últimos tres datos son números enteros. Ejemplo:
EZE Ministro-Pistarini Ezeiza Argentina 34.75 4 12 46
LAX Aeropuerto-Internacional-de-los-Angeles Los-Angeles EEUU 14 9 87 69
…
Los nodos del árbol deben tener, además del puntero izquierdo y derecho, la clave (las tres letras del código
IATA) y un puntero adonde están los datos del aeropuerto.
Luego de cargar el árbol desde el archivo, debe mostrar un menú en el cual se pueda consultar por un
aeropuerto en particular, dar de alta un nuevo aeropuerto y dar de baja a alguno.""